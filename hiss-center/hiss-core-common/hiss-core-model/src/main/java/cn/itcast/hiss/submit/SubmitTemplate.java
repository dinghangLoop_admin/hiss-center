package cn.itcast.hiss.submit;

/**
 * SubmitTemplate
 *
 * @author: wgl
 * @describe: 客户端上报接口
 * @date: 2022/12/28 10:10
 */
public interface SubmitTemplate {

    /**
     * 上报
     *
     * @return
     */
    boolean submit();
}
