import { request } from "@/utils/request";
import type { ListResult } from "@/api/model/listModel";

export function getProcessList(params) {
  return request.post<ListResult>({
    url: '/v1/receiver',
    data: params
  })
}

export function getProcessCategoryList(params) {
  return request.post<any>({
    url: '/v1/receiver',
    data: params
  })
}

export function saveProcessCategory(params) {
  return request.post<any>({
    url: '/v1/receiver',
    data: params
  })
}

export function delProcessCategory(params) {
  return request.post<any>({
    url: '/v1/receiver',
    data: params
  })
}

export function delProcess(params) {
  return request.post<any>({
    url: '/v1/receiver',
    data: params
  })
}

// 获取流程示例列表
export function getProcessInstanceList(params) {
  return request.post<any>({
    url: '/v1/receiver',
    data: params
  })
}

// 删除流程实例
export function delProcessInstance(params) {
  return request.post<any>({
    url: '/v1/receiver',
    data: params
  })
}
