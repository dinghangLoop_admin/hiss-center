import Input from './Input.vue'
import TextArea from './TextArea.vue'
import Radio from './Radio.vue'
import Checkbox from './Checkbox.vue'
import Select from './Select.vue'
import TimeSelect from './TimeSelect.vue'
import NumberInput from './NumberInput.vue'
import DateArea from './DateArea.vue'
import Rate from './Rate.vue'
import Switch from './Switch.vue'
import Slider from './Slider.vue'
import FileUpload from './FileUpload.vue'
import ImgUpload from './ImgUpload.vue'
import Position from './Position.vue'
import ProvincesArea from './ProvincesArea.vue'
import DivisionLine from './DivisionLine.vue'
import Button from './Button.vue'
import Department from './Department.vue'
import Member from './Member.vue'

export {
  Input,
  TextArea,
  Radio,
  Checkbox,
  Select,
  TimeSelect,
  NumberInput,
  DateArea,
  Rate,
  Switch,
  Slider,
  FileUpload,
  ImgUpload,
  Position,
  ProvincesArea,
  DivisionLine,
  Button,
  Department,
  Member
}
