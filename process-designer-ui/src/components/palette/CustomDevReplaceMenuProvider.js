import ReplaceMenuProvider, {Translate} from 'bpmn-js/lib/features/popup-menu/ReplaceMenuProvider';

import {
  isEventSubProcess
} from 'bpmn-js/lib/util/DiUtil';

import {
  isAny
} from 'bpmn-js/lib/features/modeling/util/ModelingUtil';

import {
  DISABLE_REPLACE_ELEMENTS as disableElement
} from './Options';

export default class CustomReplaceMenuProvider extends ReplaceMenuProvider {
  constructor(bpmnFactory,popupMenu, modeling, moddle, bpmnReplace, rules, translate) {
    super(bpmnFactory,popupMenu, modeling, moddle, bpmnReplace, rules, translate);
  }

  _createEntries(element, replaceOptions) {
    let options = ReplaceMenuProvider.prototype._createEntries.call(this, element, replaceOptions);
    for (let i = 0; i < disableElement.length; i++) {
      delete options[disableElement[i]]
    }
    return options;
  }

  getHeaderEntries(element) {
    return [];
  }
}

CustomReplaceMenuProvider.$inject = [
  'bpmnFactory',
  'popupMenu',
  'modeling',
  'moddle',
  'bpmnReplace',
  'rules',
  'translate'
];
