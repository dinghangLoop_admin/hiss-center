import {FieldProps} from "@/components/business/util";
import Constants from "@/components/business/constants";
const component = {
  id: 'starterFixedPeople',
  component: FieldProps,
  filedName:Constants.hissBusinessStarterFixedUser,
  filedType:'string'
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}

export default function(element) {
  component['element']=element;
  return component;
}
