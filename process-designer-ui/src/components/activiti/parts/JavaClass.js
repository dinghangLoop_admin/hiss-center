import {isTextFieldEntryEdited, TextFieldEntry} from "@bpmn-io/properties-panel";
import {useService} from "bpmn-js-properties-panel";

const component = {
  id: 'javaClass',
  component: JavaClass,
  isEdited: isTextFieldEntryEdited
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}
export default function(element) {
  component['element']=element;
  return component;
}

function JavaClass(props) {
  const {
    element,
    businessObject,
    id = 'javaClass'
  } = props;
  const commandStack = useService('commandStack');
  const translate = useService('translate');
  const debounce = useService('debounceInput');
  const getValue = () => {
    return businessObject.get('activiti:class');
  };
  const setValue = value => {
    commandStack.execute('element.updateModdleProperties', {
      element,
      moddleElement: businessObject,
      properties: {
        'activiti:class': value || ''
      }
    });
  };
  const validate = value => {
    if(!value){
      return translate('value required')
    }
  }
  return TextFieldEntry({
    element,
    id:id+"-javaClass",
    label: translate('Java class'),
    getValue,
    setValue,
    debounce,
    validate
  });
}
