import {isTextFieldEntryEdited, SelectEntry} from "@bpmn-io/properties-panel";
import {useService} from "bpmn-js-properties-panel";
import {is} from "bpmn-js/lib/util/ModelUtil";
import {jsx} from "@bpmn-io/properties-panel/preact/jsx-runtime";

const component = {
  id: 'onTransaction',
  component: OnTransaction,
  isEdited: isTextFieldEntryEdited
}

if(window['register']) {
  window['register'].regComponent(component.id,component)
}
export default function(element) {
  component['element']=element;
  return component;
}

function OnTransaction(props) {
  const { element, id,listener } = props;
  const translate = useService('translate');
  const bpmnFactory = useService('bpmnFactory');
  const commandStack = useService('commandStack');
  function getValue() {
    return listener.get('onTransaction');
  }
  function setValue(value) {
    commandStack.execute('element.updateModdleProperties', {
      element,
      moddleElement: listener,
      properties:{
        onTransaction:value
      }
    });
  }
  function getOptions() {
    return [{
      value: '',
      label: translate('<none>')
    },{
      value: 'before-commit',
      label: translate('before-commit')
    }, {
      value: 'committed',
      label: translate('committed')
    }, {
      value: 'rolled-back',
      label: translate('rolled-back')
    }];
  }
  return jsx(SelectEntry, {
    id: id,
    label: translate('OnTransaction'),
    getValue: getValue,
    setValue: setValue,
    getOptions: getOptions
  });
}
