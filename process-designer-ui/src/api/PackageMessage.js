
export function packageMessage(messageId,data,messageAuth) {
    let auth = messageAuth || {}
    return {
        id: messageId,
        messageAuth: {
            tenant: 'tenant_hiss',
            ...auth
        },
        palyload: data
    }
}
